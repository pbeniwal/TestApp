package com.edureka.TestApp;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AppTest 
{
    @Test
    void testHelloEdureka() throws Exception  {
		
	WebDriver driver;
	    
	FirefoxOptions options = new FirefoxOptions();
        
        options.addArguments("--headless");
	    
        String mygecko=System.getenv("HOME") + "/Downloads/geckodriver";

	System.out.println(mygecko);

        System.setProperty("webdriver.gecko.driver",mygecko);
        
        driver = new FirefoxDriver(options);
        
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

	// You will need to change IP 

        driver.get("http://164.52.198.132:8080/HelloWorld");
        
       // Thread.sleep(5000);
        
	String text = "Hello World";
	    
	int l = text.length();

	String message = "Expected String " + text +  " not found";
	    
	String bodyText = driver.findElement(By.tagName("body")).getText();

        Assert.assertEquals(bodyText.substring(0, l),text, message);
        
        driver.quit();
	}
}
